import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FavoritePage } from '../favorite/favorite';
import { LibraryPage } from '../library/library';


/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tab',
  template: `

  <ion-tabs>

    <ion-tab [root]="libraryPage" tabTitle="Library" tabIcon="book"></ion-tab>
    <ion-tab [root]="favoritePage" tabTitle="Favorite" tabIcon="star"></ion-tab>

  </ion-tabs>

  `

})
export class TabPage {

  libraryPage = LibraryPage;
  favoritePage = FavoritePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('TABPAGE JALAN');
  }

}
